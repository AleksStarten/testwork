module.exports = function () {
	let card = document.querySelectorAll('.btn-popup')
  let popups = document.querySelectorAll('.popup')
  const body = document.querySelector('body')
	if(card.length>0){
		card.forEach(item=>{
			item.addEventListener('click', function(e){
        popups.forEach(popup=>{
          if(e.target.dataset.id==popup.dataset.id&&!popup.classList.contains('popup--open')){
            popup.classList.add('popup--open')
            body.style.overflow='hidden'
          }

        })
      let popupopen = document.querySelector('.popup--open') 
      if(popupopen){
        const close = popupopen.querySelector('.popup__close')
        popupopen.addEventListener('click', function(e){
          if(!e.target.closest('.feedback')){
            popupopen.classList.remove('popup--open')
            body.style.overflow='auto'
          }
        })
        close.addEventListener('click', function(){
          popupopen.classList.remove('popup--open')
          body.style.overflow='auto'
        })
      }  
				if(!document.querySelector('.popup--open')){
					const body = document.querySelector('body')
					body.style.overflow='hidden'
					const popup = document.querySelector('.popup--open')
					let webp = popup.querySelector('.webp')
					webp.setAttribute('srcset', this.dataset.srcwebp)
					let jpg = popup.querySelector('source.jpg')
					jpg.setAttribute('srcset', this.dataset.src)
					let imgjpg = popup.querySelector('img.imgjpg')
					imgjpg.setAttribute('src', this.dataset.src)
					
				}
			})
			
		})
	}
}