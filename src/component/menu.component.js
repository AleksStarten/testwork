import { Component } from "../core/component";

export class MenuComponent extends Component{
	constructor(id){
		super(id)
	}
	init(){
		if(this.$el){
			const menu = this.$el.querySelector('.menu')
			const fullScreen = this.$el.querySelector('.menu__fullScreen')
			const header = document.querySelector('header')
			const menuList = menu.querySelector('.menu__wrapper')
			const btnClose = fullScreen.querySelector('.menu__close')
			const btnBars = menu.querySelector('.menu__btn-bars')
			const items = fullScreen.querySelectorAll('.menu__item a')
			const body = document.querySelector('body')
			if(menu){
				btnBars.addEventListener('click', openMenu.bind(this, menuList, btnBars, btnClose, body, fullScreen))
				btnClose.addEventListener('click', closeMenu.bind(this, menuList, btnBars, btnClose, body, fullScreen))
				let scrollPrev = 0
				window.addEventListener('scroll', function() {
					let scrolled = window.pageYOffset
					if(/MSIE \d|Trident.*rv:/.test(navigator.userAgent)){
						scrolled = document.documentElement.pageYOffset
					}
					if ( scrolled > 100 && scrolled > scrollPrev ) {
						header.classList.add('out')
					}
					else {
						header.classList.remove('out');
					}
					scrollPrev = scrolled;
				});
				
				items.forEach(item=>{
					console.log(item)
					item.addEventListener('click', (event)=>{scrollIntoLocation(event, menuList, btnBars, btnClose, body, fullScreen)})
				})
			}
		}
		
	}
}
const active = ($block) => {
	const active = $block.classList[0]+'_active'
	$block.classList.toggle(active)
}

const openMenu = (menuList, btnBars, btnClose, body, fullScreen) => {
	fullScreen.classList.remove('menu__fullScreen_close')
	fullScreen.style.display='flex'
	active(fullScreen)
	body.style.overflow='hidden'
	btnBars.classList.add('menu__btn-bars_delete')
}

const closeMenu = (menuList, btnBars, btnClose, body, fullScreen) => {
	active(fullScreen)
	fullScreen.classList.add('menu__fullScreen_close')
	body.style.overflow='auto'
	setTimeout(function(){
		fullScreen.style.display='none'
	}, 800)
	btnBars.classList.remove('menu__btn-bars_delete')
}

const scrollIntoLocation =(event, menuList, btnBars, btnClose, body, fullScreen)=>{
	const link = event.target
	const blockID = link.getAttribute('href').substr(1)
	const menuListActive = document.querySelector(`.${fullScreen.classList[0]}_active`)
	
	if(document.getElementById(blockID)){
		event.preventDefault()
		const yOffset = -70; 
		const element = document.getElementById(blockID);
		const y = element.getBoundingClientRect().top + window.pageYOffset + yOffset;
		window.scrollTo({top: y, behavior: 'smooth'});

	}
	if(menuListActive){
		
		fullScreen.classList.remove(`${fullScreen.classList[0]}_active`)
		fullScreen.style.display='none'
		btnBars.classList.remove('menu__btn-bars_delete')
		body.style.overflow='auto'
	}

}