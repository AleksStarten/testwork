module.exports = function () {
  const checkbox = document.querySelector('input[type="checkbox"]')
  const submit = document.querySelector('input[type="submit"]')
  if(checkbox&&submit){
    checkbox.addEventListener('click', function(){
      if(checkbox.checked){
        submit.disabled=false
      }
      else{
        submit.disabled=true
      }
    }.bind(checkbox, submit))
  }
}
