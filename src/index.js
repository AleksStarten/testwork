import './sass/style.sass';
import './component/polyfills.component'

import {MenuComponent} from './component/menu.component'
import findHeightMenu from './component/height-menu.component'
import addActiveCheckbox from './component/checkbox.component'
import addActivePopup from './component/popup.component'
import maskPhone from './component/maskPhone.component'

const menu = new MenuComponent('main-menu')
findHeightMenu()
addActiveCheckbox()
addActivePopup()



maskPhone('#phoneV-1')